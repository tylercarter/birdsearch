<?php

Route::get('/search/', 'SearchAPIController@index');
Route::apiResource('saves', 'SavedTweetController')->only([
	'index', 'store', 'destroy'
])->parameters([
    'saves' => 'tweet'
]);

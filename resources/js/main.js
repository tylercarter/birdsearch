window.birdSearch = (function(window){
	var obj = {};

	obj.updateSearchResults = function(terms){

		if( obj.cache.has(terms) ){
			return obj.paintResults(obj.cache.get(terms));
		}

		obj.tweets.saves.all(function(data){
			var saved_tweets = pluck(data, "tweet_id");

			obj.tweets.search(terms, function(data){
				data = obj.parseTweets(data, saved_tweets);
				obj.cache.store(terms, data);
				obj.paintResults(data);
			});
		});

	};

	obj.updateSavedTweets = function(){

		obj.tweets.saves.all(function(data){
			var saved_tweets = pluck(data, "tweet_id");
			data = obj.parseTweets(data, saved_tweets);
			obj.cache.store("saved", data);

			if(data.length > 0 ){
					obj.paintResults(data);
			}

		});

	};

	obj.parseTweets = function(data, saved_tweets){

		var results = [];
		for( i in data ){
			var item = data[i];
			var result = {
				'tweet_id' : "tweet:" + item.id_str,
				'text'     : item.text,
				'saved'    : false
			}

			if( "tweet_id" in item ){
				result.tweet_id = item.tweet_id;
			}

			if( "user" in item ){
					result.screen_name = item.user.screen_name;
					result.profile_image_url = item.user.profile_image_url;
			} else {
				result.screen_name = item.author;
				result.profile_image_url = item.profile_image_url;
			}

			if( "created_on" in item ){
				result.created_on = item.created_on;
			} else {
				result.created_on = item.created_at;
			}

			if ( saved_tweets.includes( result.tweet_id + "" ) ){
				result.saved = true;
			}

			results.push(result);
		}

		return results;
	}

	obj.paintResults = function(data){
		var list = $("<ul>").addClass("list-group").attr("id", "results");
		for( i in data){
			var tweet = data[i];

			var item = $("<li>")
				.addClass("list-group-item");

			$("<span>")
				.addClass("text")
				.text(tweet.text)
				.appendTo(item);

			$("<span>")
				.addClass("author")
				.text(tweet.screen_name + " wrote this ")
				.appendTo(item);

			$("<time>")
				.addClass("date")
				.attr("datetime", tweet.created_on)
				.attr("link", "https://twitter.com/"
						+ tweet.screen_name + "/status/"
						+ tweet.tweet_id.replace("tweet:", "")
				)
				.click(function(){
					location.href = $(this).attr("link");
				})
				.text(tweet.created_on)
				.appendTo(item);

			$("<img>")
				.attr("src", tweet.profile_image_url)
				.prependTo(item);

			if( tweet.saved == false ){
				$("<a>")
					.addClass("save-tweet")
					.text("(+)")
					.click(generateSaveCallback(
						tweet.tweet_id,
						tweet.text,
						tweet.screen_name,
						tweet.profile_image_url,
						tweet.created_on
					))
					.prependTo(item);
			} else {
				$("<a>")
					.addClass("remove-tweet")
					.text("(-)")
					.click(generateRemoveCallback(
						tweet.tweet_id
					))
					.prependTo(item);
			}

			list.append(item);
		}
		$("#results").replaceWith(list);

		jQuery("time").timeago();
	}

	function generateSaveCallback(id, text, author, profile_image_url, created_on){
		return function(){
			obj.tweets.saves.create(id, text, author, profile_image_url, created_on);
			$(this).html("(&check;)");
		};
	}

	function generateRemoveCallback(id){
		return function(){
			obj.tweets.saves.destroy(id);
			$(this).html("(&check;)");
		};
	}

	function pluck(array, key) {
	  return array.map(o => o[key]);
	}

	obj.cache = {
		"data" : {},
		"has" : function(key){
			if ( key in obj.cache.data ){
				return true;
			} else {
				return false;
			}
		},
		"store" : function(key, data){
			obj.cache.data[key] = data;
		},
		"get" : function(key){
			return obj.cache.data[key];
		}

	}

	obj.tweets = {
		"search" : function(terms, callback){

			if(terms == ""){
				return {};
			}

			window.$.ajax({
			  dataType: "json",
			  url: '/api/search/',
			  data: {
					'query' : terms
				},
			  success: function(response){
					callback(response.data);
				}
			});

		},

		"saves" : {
			"all" : function(callback){

				window.$.ajax({
				  dataType: "json",
				  url: '/api/saves/',
				  data: {},
				  success: function(response){
						callback(response.data);
					}
				});

			},

			"create" : function(id, text, author, profile_image_url, created_on){
				window.$.ajax({
					type: "POST",
				  url: '/api/saves/',
				  data: {
						'tweet_id' : id,
						'text' : text,
						'author' : author,
						'profile_image_url' : profile_image_url,
						'created_on' : created_on
					},
				  success: function(response){
						console.log("Saved");
					}
				});
			},

			"destroy" : function(save_id){
				window.$.ajax({
					type: "DELETE",
				  url: '/api/saves/' + save_id,
				  success: function(response){
						console.log("Destroyed");
					}
				});
			},

		}
	};

	$("#search").keyup(function(){
		var terms = $(this).val();
		obj.updateSearchResults(terms);
	});

	return obj;

})(window);

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/main.js":
/*!******************************!*\
  !*** ./resources/js/main.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

window.birdSearch = function (window) {
  var obj = {};

  obj.updateSearchResults = function (terms) {
    if (obj.cache.has(terms)) {
      return obj.paintResults(obj.cache.get(terms));
    }

    obj.tweets.saves.all(function (data) {
      var saved_tweets = pluck(data, "tweet_id");
      obj.tweets.search(terms, function (data) {
        data = obj.parseTweets(data, saved_tweets);
        obj.cache.store(terms, data);
        obj.paintResults(data);
      });
    });
  };

  obj.updateSavedTweets = function () {
    obj.tweets.saves.all(function (data) {
      var saved_tweets = pluck(data, "tweet_id");
      data = obj.parseTweets(data, saved_tweets);
      obj.cache.store("saved", data);

      if (data.length > 0) {
        obj.paintResults(data);
      }
    });
  };

  obj.parseTweets = function (data, saved_tweets) {
    var results = [];

    for (i in data) {
      var item = data[i];
      var result = {
        'tweet_id': "tweet:" + item.id_str,
        'text': item.text,
        'saved': false
      };

      if ("tweet_id" in item) {
        result.tweet_id = item.tweet_id;
      }

      if ("user" in item) {
        result.screen_name = item.user.screen_name;
        result.profile_image_url = item.user.profile_image_url;
      } else {
        result.screen_name = item.author;
        result.profile_image_url = item.profile_image_url;
      }

      if ("created_on" in item) {
        result.created_on = item.created_on;
      } else {
        result.created_on = item.created_at;
      }

      if (saved_tweets.includes(result.tweet_id + "")) {
        result.saved = true;
      }

      results.push(result);
    }

    return results;
  };

  obj.paintResults = function (data) {
    var list = $("<ul>").addClass("list-group").attr("id", "results");

    for (i in data) {
      var tweet = data[i];
      var item = $("<li>").addClass("list-group-item");
      $("<span>").addClass("text").text(tweet.text).appendTo(item);
      $("<span>").addClass("author").text(tweet.screen_name + " wrote this ").appendTo(item);
      $("<time>").addClass("date").attr("datetime", tweet.created_on).attr("link", "https://twitter.com/" + tweet.screen_name + "/status/" + tweet.tweet_id.replace("tweet:", "")).click(function () {
        location.href = $(this).attr("link");
      }).text(tweet.created_on).appendTo(item);
      $("<img>").attr("src", tweet.profile_image_url).prependTo(item);

      if (tweet.saved == false) {
        $("<a>").addClass("save-tweet").text("(+)").click(generateSaveCallback(tweet.tweet_id, tweet.text, tweet.screen_name, tweet.profile_image_url, tweet.created_on)).prependTo(item);
      } else {
        $("<a>").addClass("remove-tweet").text("(-)").click(generateRemoveCallback(tweet.tweet_id)).prependTo(item);
      }

      list.append(item);
    }

    $("#results").replaceWith(list);
    jQuery("time").timeago();
  };

  function generateSaveCallback(id, text, author, profile_image_url, created_on) {
    return function () {
      obj.tweets.saves.create(id, text, author, profile_image_url, created_on);
      $(this).html("(&check;)");
    };
  }

  function generateRemoveCallback(id) {
    return function () {
      obj.tweets.saves.destroy(id);
      $(this).html("(&check;)");
    };
  }

  function pluck(array, key) {
    return array.map(function (o) {
      return o[key];
    });
  }

  obj.cache = {
    "data": {},
    "has": function has(key) {
      if (key in obj.cache.data) {
        return true;
      } else {
        return false;
      }
    },
    "store": function store(key, data) {
      obj.cache.data[key] = data;
    },
    "get": function get(key) {
      return obj.cache.data[key];
    }
  };
  obj.tweets = {
    "search": function search(terms, callback) {
      if (terms == "") {
        return {};
      }

      window.$.ajax({
        dataType: "json",
        url: '/api/search/',
        data: {
          'query': terms
        },
        success: function success(response) {
          callback(response.data);
        }
      });
    },
    "saves": {
      "all": function all(callback) {
        window.$.ajax({
          dataType: "json",
          url: '/api/saves/',
          data: {},
          success: function success(response) {
            callback(response.data);
          }
        });
      },
      "create": function create(id, text, author, profile_image_url, created_on) {
        window.$.ajax({
          type: "POST",
          url: '/api/saves/',
          data: {
            'tweet_id': id,
            'text': text,
            'author': author,
            'profile_image_url': profile_image_url,
            'created_on': created_on
          },
          success: function success(response) {
            console.log("Saved");
          }
        });
      },
      "destroy": function destroy(save_id) {
        window.$.ajax({
          type: "DELETE",
          url: '/api/saves/' + save_id,
          success: function success(response) {
            console.log("Destroyed");
          }
        });
      }
    }
  };
  $("#search").keyup(function () {
    var terms = $(this).val();
    obj.updateSearchResults(terms);
  });
  return obj;
}(window);

/***/ }),

/***/ 0:
/*!************************************!*\
  !*** multi ./resources/js/main.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/tylercarter/Code/birdsearch/resources/js/main.js */"./resources/js/main.js");


/***/ })

/******/ });
<?php
namespace App;
use App\TwitterSearch;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;

class TwitterSearch
{

	const ENDPOINT = 'https://api.twitter.com/1.1/';

	/**
	 * Plain text search terms
	 * @var string
	 */
	protected $text = '';

	/**
	 * Language desired tweets should be in via ISO 639-1 code
	 * @var string
	 */
	protected $language = 'en';

	/**
	 * Date tweets should be before in YYYY-MM-DD syntax
	 * @var string
	 */
	protected $date = '';

	/**
	 * Number of tweets to return
	 * @var int
	 */
	protected $count = '';

	/**
	 * HTTP Client
	 * @var Client
	 */
	protected $client;

	public function __construct(){

		$this->client = new Client([
			'base_uri' => self::ENDPOINT
		]);

	}

	/**
	 * Search for tweet with the given text.
	 * @param  string $terms Text to search for
	 * @return TwitterSearch
	 */
	public function withText($terms){
		$this->text = $terms;
		return $this;
	}

	/**
	 * Search for tweet with the language.
	 * @param  string $terms ISO 639-1 code
	 * @return TwitterSearch
	 */
	public function inLanguage($language){
		$this->language = $language;
		return $this;
	}

	/**
	 * Search for a tweet created before the given date.
	 * @param  string $date YYYY-MM-DD formatted date
	 * @return TwitterSearch
	 */
	public function untilDate($date){
		$this->date = $date;
		return $this;
	}

	/**
	 * Set the number of tweets to take from the results.
	 * @param  int $count Number of tweet to return
	 * @return TwitterSearch
	 */
	public function take(int $count){
		$this->count = $count;
		return $this;
	}

	/**
	 * String to use for caching result
	 * @return string
	 */
	public function getCacheKey(){
		$query_vars = json_encode( $this->getQuery() );
		return md5( $query_vars );
	}

	/**
	 * Returns all available query parameters
	 * @return array
	 */
	protected function getQuery(){
		return [
			'q' => $this->text,
			'lang' => $this->language,
			'count' => $this->count,
			'until' => $this->date,
		];
	}

	/**
	 * Runs the search and returns the results.
	 * @return stdClass Result
	 */
	public function get(){

		$query = $this->getQuery();

		$bearer_token = Cache::remember('access_token', 60, function(){
			return $this->authenticate();
		});

		$response = $this->client->request('GET', 'search/tweets.json',
			[
				'query' => $query,
				'headers' => [
					'Authorization' => 'Bearer ' . $bearer_token
				],
			]
		);
		$content = $response->getBody()->getContents();

		return json_decode($content);
	}

	/**
	 * Authenticates the application with the Twitter API
	 * @return string Bearer token used for authenticating future requests
	 */
	protected function authenticate(){
		$consumer_key = config('app.twitter_api_key');
		$consumer_secret_key = config('app.twitter_api_secret_key');

		$encoded = base64_encode( urlencode($consumer_key) . ':' . urlencode( $consumer_secret_key ) );

		$response = $this->client->request('POST', '/oauth2/token', [
			'headers' => [
				'Authorization' => 'Basic ' . $encoded,
				'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8',
				'User-Agent' => 'BirdSearch v0.1'
			],
			'body' => 'grant_type=client_credentials',
		]);

		$content = json_decode( $response->getBody()->getContents() );
		$access_token = $content->access_token;

		return $access_token;
	}
}

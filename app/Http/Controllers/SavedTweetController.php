<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SavedTweet;

class SavedTweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tweets = SavedTweet::all();
        return response()->json([
          'data' => $tweets
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'tweet_id' => 'required|unique:saved_tweets',
          'text' => 'required',
          'author' => 'required',
          'profile_image_url' => 'required',
          'created_on' => 'required'
        ]);

        $tweet = new SavedTweet( $request->all() );
        $tweet->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
      $tweet = SavedTweet::where('tweet_id', $id)->delete();
    }
}

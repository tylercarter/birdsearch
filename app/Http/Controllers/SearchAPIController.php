<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use App\TwitterSearch;

class SearchAPIController extends Controller
{
    /**
     * Searches Twitter for the given query
     * @param  Request       $request HTTP Request Object
     * @param  TwitterSearch $search  Base Twitter Search Object
     * @return Response               JSON Response
     */
    public function index(Request $request, TwitterSearch $search){

      $request->validate([
        'query' => 'required'
      ]);

      $search->withText($request->input('query'));

      $cache_key = 'search:' . $search->getCacheKey();
      $cached = true;

      $results = Cache::remember($cache_key, 30, function() use($search, &$cached){
        $cached = false;
        return $search->get();
      });

			return response()->json([
				'data' => $results->statuses,
        'cached' => $cached
			]);
		}
}

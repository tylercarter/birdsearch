<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedTweet extends Model
{
    /**
     * Properties that can be automatically filled when submitted
     * @var array
     */
    protected $fillable = [
			'tweet_id',
			'text',
			'author',
			'profile_image_url',
			'created_on'
		];

    public function getRouteKeyName()
    {
      return 'tweet_id';
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_tweets', function (Blueprint $table) {
            $table->increments('save_id');
            $table->text('tweet_id')->unique();
            $table->text("author");
            $table->text("text");
            $table->text("profile_image_url");
            $table->text("created_on");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saved_tweets');
    }
}
